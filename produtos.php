<?php
require __DIR__ . "/vendor/autoload.php";
$perfect = new \Amaro\Products();

try {
    $perfect->getProductsByFile($argv[1]);
} catch (\Exception $exception) {
    exit($exception->getMessage());
}

$perfect->verifyProductTag();
$perfect->exportProductsToTxt();