<?php
require "bootstrap.php";
$perfect = new \Amaro\PerfectSquare();
try {
    $perfect->getSquareByFile($argv[1]);
    if ($perfect->verifySquare()) {
        exit("Perfect Square");
    }
} catch (\Exception $exception) {
    exit($exception->getMessage());
}
exit("Imperfect Square");