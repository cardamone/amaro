<?php
require __DIR__ . "/bootstrap.php";

try {
    $similarProducts = new \Amaro\SimilarProduct($argv);
    $similarProducts->getProductsByFile();
    $similarProducts->calcSimilarity();
} catch (\Exception $exception) {
    exit($exception->getMessage());
}

$selectedProduct = $similarProducts->getSelectedProduct();
echo sprintf(
    "Os 3 produtos mais similares ao produto %s (%s) são: \n",
    $selectedProduct["id"],
    $selectedProduct["name"]
);

$resultProducts = $similarProducts->getOrderProductsBySimilarity();
foreach ($resultProducts as $key => $product) {
    echo sprintf(
        "- %s (%s) com S=%s \n",
        $product["id"],
        $product["name"],
        number_format($product["similarity"], 2, '.', '')
    );
}