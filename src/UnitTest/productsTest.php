<?php

namespace Amaro\UnitTest;

use PHPUnit\Framework\TestCase;

class productsTest extends TestCase
{

    public function testGetProductsFileSuccess()
    {
        $productClass = new \Amaro\Products();
        $productClass->getProductsByFile(__DIR__ . "/../../externalFiles/produtos.txt");
        $this->assertNotEmpty($productClass->products);
    }

    public function testGetProductsFileFail()
    {
        $this->expectException(\Exception::class);
        $productClass = new \Amaro\Products();
        $productClass->getProductsByFile("error.txt");
    }

    public function testVerifyProductTagSuccess()
    {
        $productClass = new \Amaro\Products();
        $productClass->products = [
            "products" => [
                [
                    "id" => 8371,
                    "name" => "VESTIDO TRICOT CHEVRON",
                    "tags" => ["balada", "neutro", "delicado", "festa"]
                ]
            ]
        ];
        $productClass->verifyProductTag();
        $this->assertArrayHasKey("vectorTags", $productClass->products[8371]);
    }
}