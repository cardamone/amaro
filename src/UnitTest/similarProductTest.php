<?php

namespace Amaro\UnitTest;

use PHPUnit\Framework\TestCase;

class similarProductTest extends TestCase
{

    public function testGetProductsByFileSuccess()
    {
        $similarProductClass = new \Amaro\SimilarProduct([1 => "param"]);
        $similarProductClass->getProductsByFile();
        $this->assertNotEmpty($similarProductClass->products);
    }

    public function testVerifyProductTagSuccess()
    {
        $productClass = new \Amaro\Products();
        $productClass->products = [
            "products" => [
                [
                    "id" => 8371,
                    "name" => "VESTIDO TRICOT CHEVRON",
                    "tags" => ["balada", "neutro", "delicado", "festa"]
                ]
            ]
        ];
        $productClass->verifyProductTag();
        $this->assertArrayHasKey("vectorTags", $productClass->products[8371]);
    }

    public function testGetSelectedProduct()
    {
        $similarProductClass = new \Amaro\SimilarProduct([1 => 8371]);
        $similarProductClass->products = [
            8371 => [
                "id" => 8371,
                "name" => "VESTIDO TRICOT CHEVRON",
                "tags" => ["balada", "neutro", "delicado", "festa"]
            ]
        ];
        $response = $similarProductClass->getSelectedProduct();
        $this->assertNotEmpty($response);
    }

    public function testGetSelectedProductFail()
    {
        $this->expectException(\Exception::class);
        $similarProductClass = new \Amaro\SimilarProduct([1 => 8371]);
        $similarProductClass->getSelectedProduct();
    }

    public function testPerfectCalcSimilaritySuccess()
    {
        $similarProductClass = new \Amaro\SimilarProduct([1 => 8371]);
        $similarProductClass->products = [
            8371 => [
                "id" => 8371,
                "name" => "VESTIDO TRICOT CHEVRON",
                "tags" => ["balada", "neutro", "delicado", "festa"],
                "vectorTags" => [
                    "neutro" => true,
                    "veludo" => false,
                    "couro" => false,
                    "basics" => false,
                    "festa" => true,
                    "workwear" => false,
                    "inverno" => false,
                    "boho" => false,
                    "estampas" => false,
                    "balada" => true,
                    "colorido" => false,
                    "casual" => false,
                    "liso" => false,
                    "moderno" => false,
                    "passeio" => false,
                    "metal" => false,
                    "viagem" => false,
                    "delicado" => true,
                    "descolado" => false,
                    "elastano" => false,
                ]
            ],
            123 => [
                "id" => 123,
                "name" => "Produto Certo",
                "tags" => ["balada", "neutro", "delicado", "festa"],
                "vectorTags" => [
                    "neutro" => true,
                    "veludo" => false,
                    "couro" => false,
                    "basics" => false,
                    "festa" => true,
                    "workwear" => false,
                    "inverno" => false,
                    "boho" => false,
                    "estampas" => false,
                    "balada" => true,
                    "colorido" => false,
                    "casual" => false,
                    "liso" => false,
                    "moderno" => false,
                    "passeio" => false,
                    "metal" => false,
                    "viagem" => false,
                    "delicado" => true,
                    "descolado" => false,
                    "elastano" => false,
                ]
            ]
        ];
        $similarProductClass->calcSimilarity();
        $this->assertEquals(1, $similarProductClass->products[123]["similarity"]);
    }

    public function testNotPerfectCalcSimilaritySuccess()
    {
        $similarProductClass = new \Amaro\SimilarProduct([1 => 8371]);
        $similarProductClass->products = [
            8371 => [
                "id" => 8371,
                "name" => "VESTIDO TRICOT CHEVRON",
                "tags" => ["balada", "neutro", "delicado", "festa"],
                "vectorTags" => [
                    "neutro" => true,
                    "veludo" => false,
                    "couro" => false,
                    "basics" => false,
                    "festa" => true,
                    "workwear" => false,
                    "inverno" => false,
                    "boho" => false,
                    "estampas" => false,
                    "balada" => true,
                    "colorido" => false,
                    "casual" => false,
                    "liso" => false,
                    "moderno" => false,
                    "passeio" => false,
                    "metal" => false,
                    "viagem" => false,
                    "delicado" => true,
                    "descolado" => false,
                    "elastano" => false,
                ]
            ],
            123 => [
                "id" => 123,
                "name" => "Produto Certo",
                "tags" => ["balada", "neutro", "delicado", "festa"],
                "vectorTags" => [
                    "neutro" => true,
                    "veludo" => false,
                    "couro" => false,
                    "basics" => false,
                    "festa" => true,
                    "workwear" => false,
                    "inverno" => false,
                    "boho" => false,
                    "estampas" => false,
                    "balada" => true,
                    "colorido" => false,
                    "casual" => false,
                    "liso" => false,
                    "moderno" => true,
                    "passeio" => false,
                    "metal" => false,
                    "viagem" => false,
                    "delicado" => true,
                    "descolado" => false,
                    "elastano" => false,
                ]
            ]
        ];
        $similarProductClass->calcSimilarity();
        $this->assertNotEquals(1, $similarProductClass->products[123]["similarity"]);
    }

    public function testGetOrderProductsBySimilaritySuccess()
    {
        $similarProductClass = new \Amaro\SimilarProduct([1 => 8371]);
        $similarProductClass->products = [
            8371 => [
                "id" => 8371,
                "similarity" => 0.1
            ],
            234 => [
                "id" => 234,
                "similarity" => 0.5
            ],
            123 => [
                "id" => 123,
                "similarity" => 1
            ]
        ];
        $response = $similarProductClass->getOrderProductsBySimilarity();
        $this->assertArrayNotHasKey(8371, $response);
        $lastArray = end($response);
        $this->assertEquals(234, $lastArray['id']);
    }
}