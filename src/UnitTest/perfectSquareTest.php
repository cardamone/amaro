<?php

namespace Amaro\UnitTest;

use PHPUnit\Framework\TestCase;

class perfectSquareTest extends TestCase
{

    public function testGetSquareFileSuccess()
    {
        $perfect = new \Amaro\PerfectSquare();
        $response = $perfect->getSquareByFile(__DIR__ . "/../../externalFiles/quadrado.txt");
        $this->assertNotEmpty($response);
    }

    public function testGetSquareFileFail()
    {
        $this->expectException(\Exception::class);
        $perfect = new \Amaro\PerfectSquare();
        $perfect->getSquareByFile("error.txt");
    }

    public function testPerfectSquareSuccess()
    {
        $perfect = new \Amaro\PerfectSquare();
        $perfect->squareData = [
            '1 1 1',
            '1 1 1',
            '1 1 1'
        ];
        $response = $perfect->verifySquare();
        $this->assertTrue($response);
    }

    public function testPerfectSquareFail()
    {
        $this->expectException(\Exception::class);
        $perfect = new \Amaro\PerfectSquare();
        $perfect->squareData = [
            '1 1 1',
            '1 1 1',
            '1 1 2'
        ];
        $perfect->verifySquare();
    }
}