<?php

namespace Amaro;

class PerfectSquare
{

    public $squareData = [];

    public function getSquareByFile($file)
    {
        if (!file_exists($file)) {
            throw new \Exception("File not found");
        }
        $file = fopen($file, "r");
        while (!feof($file)) {
            $square = fgets($file);

            if (!empty($square)) {
                $this->squareData[] = $square;
            }
        }
        return $this->squareData;
    }

    public function verifySquare()
    {
        $line = [];
        $sumLine = [];
        $sumColumn = [];
        $sumDiagonal = 0;
        $sumReverseDiagonal = 0;
        list($line, $sumLine) = $this->validatePerfectSquare($line, $sumLine);

        foreach ($line as $key => $lineValue) {
            $sumDiagonal += $line[$key][$key];
            $reverseIndex = count($line) - 1;
            $sumReverseDiagonal += $line[$reverseIndex - $key][$reverseIndex - $key];
            $sumColumn = $this->sumColumn($lineValue, $sumColumn);
        }

        return $sumDiagonal === $sumReverseDiagonal && $sumColumn === $sumLine;
    }

    private function validatePerfectSquare($line, $sumLine)
    {
        foreach ($this->squareData as $key => $value) {
            $line[$key] = explode(' ', trim($value));
            $sumLine[] = array_sum($line[$key]);
            if (!empty($sumLine[$key - 1]) && $sumLine[$key] != $sumLine[$key - 1]) {
                throw  new \Exception("Imperfect Square");
            }
        }
        return array($line, $sumLine);
    }

    private function sumColumn($lineValue, $sumColumn)
    {
        foreach ($lineValue as $keyColumn => $columnValue) {
            if (empty($sumColumn[$keyColumn])) {
                $sumColumn[$keyColumn] = $columnValue;
            } else {
                $sumColumn[$keyColumn] += $columnValue;
            }
        }
        return $sumColumn;
    }
}
