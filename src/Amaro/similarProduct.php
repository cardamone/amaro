<?php

namespace Amaro;

class SimilarProduct
{
    public $products = [];
    private $productId;

    public function __construct(array $argument)
    {
        if (empty($argument[1])) {
            throw new \Exception("Invalid product id");
        }
        $this->productId = $argument[1];
    }

    public function getProductsByFile()
    {
        $file = __DIR__ . '/../../externalFiles/newProducts.txt';
        if (!file_exists($file)) {
            throw new \Exception("File not found");
        }

        $file = fopen($file, "r");
        $productsFile = '';
        while (!feof($file)) {
            $productsFile .= fgets($file);
        }
        $this->products = json_decode($productsFile, true);
        return $this->products;
    }

    public function calcSimilarity()
    {
        $selectedProduct = $this->getSelectedProduct();
        foreach ($this->products as $key => $productId) {
            $result = [];
            foreach ($productId["vectorTags"] as $tagKey => $tag) {
                $subtract = $selectedProduct["vectorTags"][$tagKey] - $tag;
                $result[] = $subtract * $subtract;
            }
            $result = sqrt(array_sum($result));
            $this->products[$key]["similarity"] = 1 / (1 + $result);
        }
    }

    public function getOrderProductsBySimilarity()
    {
        unset($this->products[$this->productId]);
        usort($this->products, function ($previous, $current) {
            return $previous['similarity'] < $current['similarity'];
        });
        return array_slice($this->products, 0, 3);
    }

    public function getSelectedProduct()
    {
        if (empty($this->products[$this->productId])) {
            throw new \Exception('Invalid Product');
        }

        return $this->products[$this->productId];
    }
}