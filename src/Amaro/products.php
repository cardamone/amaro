<?php

namespace Amaro;

class Products
{
    const TAGS = [
        "neutro",
        "veludo",
        "couro",
        "basics",
        "festa",
        "workwear",
        "inverno",
        "boho",
        "estampas",
        "balada",
        "colorido",
        "casual",
        "liso",
        "moderno",
        "passeio",
        "metal",
        "viagem",
        "delicado",
        "descolado",
        "elastano"
    ];
    public $products = [];

    public function getProductsByFile($file)
    {
        if (!file_exists($file)) {
            throw new \Exception("File not found");
        }
        $file = fopen($file, "r");
        $productsFile = '';
        while (!feof($file)) {
            $productsFile .= fgets($file);
        }

        $this->products = json_decode($productsFile, true);
        if (empty($this->products["products"])) {
            throw new \Exception("Invalid product");
        }
    }

    public function exportProductsToTxt()
    {
        $productFile = fopen(
            __DIR__ . "/../../externalFiles/newProducts.txt",
            "w"
        ) or die("Unable to open file!");

        fwrite($productFile, json_encode($this->products));
        fclose($productFile);
    }

    public function verifyProductTag()
    {
        $newProduct = [];
        foreach ($this->products["products"] as $productKey => $product) {
            $this->createVectorTags($product, $newProduct);
        }

        $this->products = $newProduct;
    }

    public function createVectorTags($product, &$newProduct)
    {
        $newProduct[$product["id"]] = $product;
        foreach (self::TAGS as $key => $tag) {
            $newProduct[$product["id"]]["vectorTags"][$tag] = in_array($tag, $product["tags"]);
        }

        return $newProduct;
    }
}