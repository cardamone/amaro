# Teste Amaro

* Clonar o repositorio git@bitbucket.org:cardamone/amaro.git
* E necessario instalar o package manager Composer (https://getcomposer.org/)
* Instalar dependencias: $ composer install
* Composer Dump: composer dump-autoload -o
* Teste do quadrado perfeito:
    `$ php quadradoPerfeito.php externalFiles/quadrado.txt`
* Gerar arquivo de produtos com o vetor de tags:
    `$ php produtos.php externalFiles/produtos.txt`
* Verificar semelhanca entre produtos:
    `$ php buscarProdutosSimilares.php <id do produto>`
* testes unitarios:
    `$ ./vendor/bin/phpunit`